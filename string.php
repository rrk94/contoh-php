<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
       echo "<h3> Soal No 1</h3>";
       $string = "PHP is never old";
       echo "Kalimat Pertama :". $string. "<br>";
       echo "Panjang string :" .strlen($string). "<br>";
       echo "Jumlah kata : " .str_word_count($string). "<br><br>";   

       echo "<h3> Soal No 2</h3>";
       $string2 = "I love PHP";
       echo "<label>String: </label> \"$string2\" <br>";
       echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;
       echo "Kata Kedua: " . substr($string2, 2, 4) . "<br>" ;
       echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br><br>" ;

       echo "<h3> Soal No 3 </h3>";
       $string3 = "PHP is old but Good!";
       echo "String:" .$string3. "<br>"; 
       echo "String:".str_replace("Good","Awesome",$string3);
      
    ?>
</body>
</html>